This command line tool is designed to simulate users using fixed instruction sets.

Simple usage:

```bash
node index.js test --users=50 delay:0,1000 file:entry-urls.txt delay:0,1000 file:product-urls.txt
```

This will give you a list of urls called together with timing and status information similar to this:

```tsv
user	instr	start	status	time	url	error
0	1	6	200	267	http://www.example.com/startpage	
1	1	1008	200	267	http://www.example.com/startpage	
0	3	2277	200	111	http://www.example.com/produkt/page1	
1	3	3297	200	111	http://www.example.com/produkt/page2	
```

The instruction syntax has the following options:

- `f:[filename]`, `file[filename]` A reference to a file with urls from which to pick one. The url will be picked pseudo-random. Every time you run the test the same user will pick the same url.
- `d:[delay]`, `d:[min-delay],[max-delay]`, `delay:[delay]`, `delay:[min-delay],[max-delay]` these will add spacing within the instructions. If you give 2 parameters than the delay will be between the min and the max value. This is again pseudo-random
- `ud:[delay]`, `user-delay:[delay]` This will add a delay specific to the user. If you have 11 users and you specify `ud:1000` than the first user will have 0ms delay, the second 100ms etc.
- `ss:[url],[suggest-url],[sword-file.txt],[min-delay],[max-delay],[min-final-delay],[max-final-delay]` This will simulate a search. The sword-file should contain possible search words. The suggester-url will be called for every character in the search word. like `[suggest-url]w`, `[suggest-url]wo`, `[suggest-url]wor`, `[suggest-url]word` with a random delay specified using `[min-delay]` and `max-delay`. After that between `[min-final-delay]` and `[max-final-delay]` will be waited and `[url]word` will be called.
