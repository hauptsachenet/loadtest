const program = require('commander');
const fs = require('fs');
const randomSeed = require('random-seed');
const request = require('request');
const { performance } = require('perf_hooks');

program
    .command('test [instructions...]')
    .description(`This command runs a load test.
        
        You pass it instructions on how every user should roughly behave.
        Your command will probably look like this:
        
        index.js test --users=100 d:0,5000 f:startpages.txt d:1000,4000 f:subpages.txt ...
        
        This test is guaranteed to be repeatable. The randomness is seeded and therefor always the same.
        The only variance is the performance of your server.
    `)
    .option('-u, --users <users>', 'The amount of users to simulate')
    .option('-o, --offset <offset>', 'Offset the first user, this is useful if you test users sequencially')
    .option('-t, --timing <timings...>', 'Uses the server-timing header to add the specified header to the list')
    .option('-r, --repeat <seconds>', 'Repeat the test for a given amount of seconds.')
    .option('-s, --summarize', 'Show a summary at the end')
    .action(function (instructions, raw) {
        const options = {
            users: raw.users || 10,
            offset: raw.offset || 0,
            timings: raw.timing.split(','),
            summarize: raw.summarize || false,
            repeatTime: raw.repeat ? raw.repeat * 1000 : false,
        };

        if (instructions.length <= 0) {
            console.error("No instructions passed");
            process.exit(1);
        }

        for (let i = 0; i < instructions.length; i++) {
            const instruction = instructions[i];
            const match = instruction.match(/^([^:]+):\s*(.+)$/);
            const command = match[1];
            const arguments = match[2].split(/,/);
            switch (command) {
                case 'ss':
                case 'search-suggest':
                    instructions[i] = {
                        type: 'search',
                        url: arguments[0],
                        suggestUrl: arguments[1],
                        swords: fs.readFileSync(arguments[2], {encoding: 'utf8'}).match(/^(?!#)[^\n]+/gm),
                        delay: arguments.slice(3, 5).map(value => parseInt(value, 10)),
                        finalDelay: arguments.slice(5, 7).map(value => parseInt(value, 10))
                    };
                    break;

                case 's':
                case 'search':
                    instructions[i] = {
                        type: 'search',
                        swords: fs.readFileSync(arguments[1], {encoding: 'utf8'}).match(/^(?!#)[^\n]+/gm),
                        url: arguments[0],
                        delay: arguments.slice(2, 4).map(value => parseInt(value, 10))
                    };
                    break;

                case 'f':
                case 'file':
                    const urls = fs.readFileSync(arguments[0], {encoding: 'utf8'}).match(/^(?!#)[^\n]+/gm);
                    if (urls.length < 0) {
                        console.error(`Failed to parse file ${instruction}`);
                        process.exit(1);
                    }

                    instructions[i] = {
                        type: 'urls',
                        urls: urls,
                    };
                    break;

                case 'd':
                case 'delay':
                    instructions[i] = {
                        type: 'delay',
                        delay: arguments.map(value => parseInt(value, 10)),
                    };
                    break;

                case 'ud':
                case 'user-delay':
                    instructions[i] = {
                        type: 'user-delay',
                        maxDelay: parseInt(arguments[0], 10),
                    };
                    break;

                default:
                    console.error(`instruction "${command}" unclear.`);
                    process.exit(1);
            }
        }

        const header = ["user", "instr", "start", "status", "time"];
        for (let i = 0; i < options.timings.length; i++) {
            const timing = options.timings[i];
            header.push('t:' + timing.substr(0, 5));
        }
        header.push("url", "error");
        console.log(header.join("\t"));

        const timings = {time: []};
        for (let i = 0; i < options.timings.length; i++) {
            const timing = options.timings[i];
            timings[timing] = [];
        }

        let usersActive = options.users - options.offset;
        const requestAgentPool = {};
        const testStartTime = performance.now();

        for (let userIndex = options.offset; userIndex < options.users; userIndex++) {
            const random = randomSeed.create('0:' + userIndex);
            let userInstruction = 0;

            function userTimeout(callback, array) {
                const delay = array.length <= 1 ? array[0] : random.intBetween(array[0], array[1]);
                setTimeout(callback, delay);
            }

            function runNextInstruction() {
                const instructionIndex = userInstruction++;
                if (options.repeatTime) {
                    const timeLeft = options.repeatTime - (performance.now() - testStartTime);
                    if (timeLeft <= 0) {
                        usersActive--;
                        if (usersActive === 0) {
                            end();
                        }
                        return; // done
                    }
                } else {
                    if (instructionIndex >= instructions.length) {
                        usersActive--;
                        if (usersActive === 0) {
                            end();
                        }
                        return; // done
                    }
                }

                function userRequest(url, callback) {
                    const startTime = performance.now();
                    request({url: url, timeout: 30000, pool: requestAgentPool}, (error, response) => {
                        const requestTime = performance.now() - startTime;
                        const row = [
                            userIndex,
                            instructionIndex,
                            Math.round(startTime - testStartTime),
                            response ? response.statusCode : null,
                            Math.round(requestTime),
                        ];

                        timings['time'].push(requestTime);

                        const serverTiming = response && response.headers['server-timing'] || '';
                        options.timings.forEach(timing => {
                            const timingMatch = serverTiming.match(new RegExp(timing + ';\\s*dur=([0-9.]+)'));
                            if (timingMatch) {
                                timings[timing].push(timingMatch[1]);
                                row.push(Math.round(timingMatch[1]));
                            } else {
                                row.push('N/A');
                            }
                        });

                        row.push(url, error);
                        console.log(row.join("\t"));

                        if (callback) {
                            callback.apply(this, arguments);
                        }
                    });
                }

                const instruction = instructions[instructionIndex % instructions.length];
                switch (instruction.type) {
                    case "search":
                        const sword = instruction.swords[random(instruction.swords.length)];

                        const requestNextCharacter = function (characters) {
                            let callback = () => {};

                            if (characters === sword.length) {
                                if (instruction.url) {
                                    callback = () => {
                                        const searchUrl = instruction.url + sword.substr(0, characters);
                                        userTimeout(() => {
                                            userRequest(searchUrl, characters === sword.length ? runNextInstruction : null);
                                        }, instruction.finalDelay);
                                    }
                                } else {
                                    callback = runNextInstruction;
                                }
                            }

                            if (instruction.suggestUrl) {
                                const suggestUrl = instruction.suggestUrl + sword.substr(0, characters);
                                userRequest(suggestUrl, callback);
                            }

                            if (characters < sword.length) {
                                // just type away even if the request has not finished
                                userTimeout(() => requestNextCharacter(characters + 1), instruction.delay);
                            }
                        };

                        requestNextCharacter(1);
                        break;

                    case "urls":
                        let randomUrl = instruction.urls[random(instruction.urls.length)];
                        userRequest(randomUrl, runNextInstruction);
                        break;

                    case "delay":
                        userTimeout(runNextInstruction, instruction.delay);
                        break;

                    case "user-delay":
                        userTimeout(runNextInstruction, [instruction.maxDelay * (userIndex / (options.users - 1))]);
                        break;
                }
            }

            // start user
            runNextInstruction();
        }

        function end() {
            if (options.summarize) {
                const avgFooter = ["", "", "", "avg"];
                Object.values(timings).forEach(values => {
                    const sum = values.reduce((a, b) => parseFloat(a) + parseFloat(b), 0);
                    avgFooter.push(sum / values.length);
                });
                console.log(avgFooter.join("\t"));

                const maxFooter = ["", "", "", "max"];
                Object.values(timings).forEach(values => {
                    maxFooter.push(Math.max(...values));
                });
                console.log(maxFooter.join("\t"));

                const minFooter = ["", "", "", "min"];
                Object.values(timings).forEach(values => {
                    minFooter.push(Math.min(...values));
                });
                console.log(minFooter.join("\t"));
            }
        }
    })
;

program.parse(process.argv);

if (program.args.length === 0) {
    program.help();
}
